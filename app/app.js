'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.drivers_table',
  'myApp.driver_profile',
  'myApp.services',
  'myApp.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
//routes
  $routeProvider.
  when("/drivers_table", {templateUrl: "views/drivers_table.html", controller: "DriversTableCtrl"}).
  when("/driver_profile/:id", {templateUrl: "views/driver_profile.html", controller: "DriverProfileCtrl"}).
  otherwise({redirectTo: '/drivers_table'});
}]);
