'use strict';

angular.module('myApp.driver_profile', ['myApp.services','ngRoute'])

.controller('DriverProfileCtrl', function($scope, $routeParams, ergastAPI) {
  $scope.id                     = $routeParams.id;
  $scope.races                  = [];
  $scope.driver                 = '';
  $scope.noDriverRacingData     = false;
  $scope.noDrivergData          = false;
  $scope.orderByField           = 'round'; //initial table order


  //get a drivers profile data
  ergastAPI.getDriverProfile($scope.id).success((response)=> {
    //this could be avoided if we construct our returned data properly
    if(response.MRData.StandingsTable &&
        response.MRData.StandingsTable.StandingsLists &&
        response.MRData.StandingsTable.StandingsLists[0] &&
        response.MRData.StandingsTable.StandingsLists[0].DriverStandings &&
        response.MRData.StandingsTable.StandingsLists[0].DriverStandings[0]) {
        $scope.driver = response.MRData.StandingsTable.StandingsLists[0].DriverStandings[0];
    } else {
      console.log('something went wrong no data');
    }

     if($scope.driver == 'undefined' || $scope.driver == 'null' || $scope.driver.length == 0) {
      $scope.noDriverData = true;
     }

  });

  //get a drivers race history
  ergastAPI.getDriverRacesHistory($scope.id).success((response)=> {
    if(response.MRData.RaceTable && response.MRData.RaceTable.Races) {
      //parsing the round, grid and position because the API is returning them as strings
      angular.forEach(response.MRData.RaceTable.Races, (value, key)=> {
        value.round                 = JSON.parse(value.round);
        value.Results[0].grid       = JSON.parse(value.Results[0].grid);
        value.Results[0].position   = JSON.parse(value.Results[0].position);
        $scope.races.push(value);
      });

    } else {
      console.log('something went wrong no data');
    }

    //set flag if there are no data
    if($scope.races == 'undefined' || $scope.races == 'null' || $scope.races.length == 0) {
      $scope.noDriverRacingData = true;
    }

  });
});
