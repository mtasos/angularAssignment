'use strict';

angular.module('myApp.drivers_table', ['myApp.services','ngRoute'])

.controller('DriversTableCtrl', function($scope, ergastAPI) {
  //search functionality
  $scope.driverNameFilter = null;
  $scope.driversList      = [];
  $scope.driversListEmpty = false;
  $scope.orderByField     = 'givenName'; //initial table order

  $scope.searchDriverFilter = (driver)=> {
      let re = new RegExp($scope.driverNameFilter, 'i');
      return !$scope.driverNameFilter || re.test(driver.Driver.givenName) || re.test(driver.Driver.familyName);
  };

  //getting data from the response
  ergastAPI.getDriversData().success((response)=> {
    //this could be avoided if we construct our returned data properly
    if(response.MRData.StandingsTable && response.MRData.StandingsTable.StandingsLists
       && response.MRData.StandingsTable.StandingsLists[0] && response.MRData.StandingsTable.StandingsLists[0].DriverStandings) {
      //parsing the wins because the API is returning them as a string
      angular.forEach(response.MRData.StandingsTable.StandingsLists[0].DriverStandings,  (value, key)=> {
        value.wins = JSON.parse(value.wins);
        $scope.driversList.push(value);
      });

    } else {
      console.log('something went wrong no data');
    }

    //set flag if there are no data
    if($scope.driversList =='undefined' || $scope.driversList =='null' || $scope.driversList.length == 0 ) {
      $scope.driversListEmpty = true;
    }

  });
});
