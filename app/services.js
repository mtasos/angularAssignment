angular.module('myApp.services', [])
  .factory('ergastAPI', function($http) {

    var ergastAPI = {};
    //fetch data for the drivers table
    ergastAPI.getDriversData = function() {
      return $http({
        method: 'JSONP',
        url: 'http://ergast.com/api/f1/2013/driverStandings.json?callback=JSON_CALLBACK'
      });
    }
    //fetch date for driver's profile
    ergastAPI.getDriverProfile = function(id) {
      return $http({
        method: 'JSONP',
        url: 'http://ergast.com/api/f1/2013/drivers/'+ id +'/driverStandings.json?callback=JSON_CALLBACK'
      });
    }
    //fetch data for divers racing history
    ergastAPI.getDriverRacesHistory = function(id) {
      return $http({
        method: 'JSONP',
        url: 'http://ergast.com/api/f1/2013/drivers/'+ id +'/results.json?callback=JSON_CALLBACK'
      });
    }
    return ergastAPI;
});
